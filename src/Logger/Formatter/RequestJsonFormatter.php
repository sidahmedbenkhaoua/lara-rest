<?php

namespace Sbe\Lararest\Logger\Formatter;

use Psr\Http\Message\RequestInterface;

class RequestJsonFormatter extends AbstractRequestFormatter
{
    /**
     * @param RequestInterface $request
     * @param array            $options
     *
     * @return array
     */
    public function format (RequestInterface $request, array $options,string $idRequest) {
        $this->options = [];

        $this->extractArguments($request, $options);
        $string ="\r\n";
        $string ="Output Message:";
        $string .="\r\n";
        $string .="--------------------------";
        $string .="\r\n";
        $string .="ID: REST-".$idRequest;
        $string .="\r\n";
        $string .="Methode:".$this->options['method'];
        $string .="\r\n";
        $string .="Address:".$this->options['url'];
        $string .="\r\n";
        //$string .="headers:".json_encode($this->options['headers']);
        $string .= "\r\n";
        $string .="Payload:";
        $string .= "\r\n";
        if(isset($this->options['data'])){
            $string .=json_encode(json_decode($this->options['data']), JSON_PRETTY_PRINT);
        }else{
            $string .="****************empty****************";
        }


        return  $string;
    }
}
