<?php

namespace Sbe\Lararest\Logger\Formatter;

use Exception;

class ExceptionJsonFormatter extends AbstractExceptionFormatter
{
    public function format (Exception $ex, array $options = [],$idRequest) {
        $this->extractArguments($ex, $options);

        return json_encode($this->options);
    }
}
