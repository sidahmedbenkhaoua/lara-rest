<?php

namespace Sbe\Lararest\Logger\Formatter;

use Psr\Http\Message\ResponseInterface;

class ResponseJsonFormatter extends AbstractResponseFormatter
{
    public function format (ResponseInterface $response, array $options = [],$idRequest) {
        $this->extractArguments($response, $options);
        $string ="\r\n";
        $string ="Inbound Message:";
        $string .="\r\n";
        $string .="--------------------------";
        $string .="\r\n";
        $string .="ID: REST-".$idRequest;
        $string .="\r\n";
        $string .="Response-Code:".$this->options['status_code'];
        $string .="\r\n";
        $string .="Protocol:".$this->options['protocol'];
        $string .="\r\n";
        $string .="headers:".json_encode($this->options['headers']['Content-Type']);
        $string .= "\r\n";
        $string .="Payload:";
        $string .= "\r\n";
        $string .=json_encode(json_decode($this->options['body']), JSON_PRETTY_PRINT);


        return  $string;
    }
}
