<?php

namespace Sbe\Lararest;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;

use Sbe\Lararest\Logger\Formatter\RequestJsonFormatter;
use Sbe\Lararest\Middlware\HttpMiddleware;
use Sbe\Lararest\Middlware\LogMiddleware;
use Sbe\Lararest\Services\Impl\ClientRestServiceImpl;


class LararestServiceProvider extends ServiceProvider
{
    public static string $CAHANNEL_WS = 'ws';


    public function register()
    {

        $this->app->singleton(ClientRestServiceImpl::class, function ($app, array $parameters) {
            $clientRest = $parameters[0];
            $this->client = new Client([
                'base_uri' => $clientRest->getServiceUrl(),
                'timeout' => $clientRest->getConnectionTimeout(),
                'connect_timeout' => $clientRest->getReadTimeout(),
                'handler' => $this->setLoggingHandler(),
            ]);

            return new ClientRestServiceImpl($this->client);


        });
    }

    /**
     * Setup Logging Handler Stack
     */
    private function setLoggingHandler()
    {
        $options = [
            'length' => 200,
            'log_request' => true,
            'log_response' => true,
            'log_level' => 'info',
            'force_json' => false,
            'request_formatter' => new RequestJsonFormatter,
            'separate' => true,
        ];

        $stack = HandlerStack::create();
        $stack->push(new LogMiddleware(Log::channel(self::$CAHANNEL_WS), $options), 'logger');
        $stack->push(new HttpMiddleware($options));
        return $stack;
    }
}
