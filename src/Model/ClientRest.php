<?php

namespace Sbe\Lararest\Model;

class ClientRest
{

    protected $options;

    public function __construct(array $options = [])
    {
        $this->options = $options;
    }

    public function getServiceName()
    {
        return isset($this->options['serviceName']) ? $this->options['serviceName'] : '';
    }

    public function getServiceUrl()
    {
        return isset($this->options['serviceUrl']) ? $this->options['serviceUrl'] : '';
    }

    public function getServiceProvider()
    {
        return isset($this->options['serviceProvider']) ? $this->options['serviceProvider'] : '';
    }

    public function getServiceVersion()
    {
        return isset($this->options['serviceVersion']) ? $this->options['serviceVersion'] : '';
    }

    public function getConnectionTimeout()
    {
        return isset($this->options['connectionTimeout']) ? $this->options['connectionTimeout'] : 500;
    }

    public function getReadTimeout()
    {
        return isset($this->options['readTimeout']) ? $this->options['readTimeout'] : 500;
    }

}
