<?php


namespace Sbe\Lararest\Middlware;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Illuminate\Support\Facades\Log;

class HttpMiddleware
{

    protected $options;

    public function __construct(array $options = [])
    {
        $this->options = $options;

    }

    public function __invoke(callable $handler)
    {
        return function (RequestInterface $request,array $options) use ($handler) {


            return $handler($request,$options)->then($this->handleSuccess($request),
                $this->handleFailure($request));


            return $handler($request, $options);
        };
    }

    /**
     * Returns a function which is handled when a request was successful.
     *
     * @param RequestInterface $request
     *
     * @return callable
     */
    private function handleSuccess(RequestInterface $request): callable
    {
        return function (ResponseInterface $response) use ($request) {
            return $response;
        };
    }

    /**
     * Returns a function which is handled when a request was rejected.
     *
     * @param RequestInterface $request
     * @param array $options
     *
     * @return callable
     */
    private function handleFailure(RequestInterface $request): callable
    {
        return function (Exception $reason) use ($request) {

            Log::info(print_r($reason,true));

            return rejection_for($reason);
        };
    }
}
