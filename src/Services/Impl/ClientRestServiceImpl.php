<?php

namespace Sbe\Lararest\Services\Impl;


use GuzzleHttp\Client;

use Sbe\Lararest\Services\ClientRestService;

class ClientRestServiceImpl implements ClientRestService
{

    private Client $clientRest;

    /**
     * ClientRestServiceImpl constructor.
     * @param Client $clientRest
     */
    public function __construct(Client $clientRest)
    {
        $this->clientRest = $clientRest;
    }


    public function Get($uri, array $header)
    {
        return $this->clientRest->request("GET", $uri, $header)->getBody();
    }

    public function GetWithParams($params)
    {
        // TODO: Implement GetWithParams() method.
    }

    public function Post($uri, $body)
    {
        return $this->clientRest->request("POST", $uri, $body)->getBody();
    }

    public function PostWithParams($parms, $body)
    {
        // TODO: Implement PostWithParams() method.
    }

    public function delete($uri,$body)
    {
        return $this->clientRest->request("DELETE", $uri, $body)->getBody();
    }
}
