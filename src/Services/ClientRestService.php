<?php

namespace Sbe\Lararest\Services;

interface ClientRestService
{

    public function Get($uri,array $header);

    public function GetWithParams($params);

    public function Post($uri,$body);

    public function PostWithParams($parms, $body);

    public function delete($uri,$body);


}
