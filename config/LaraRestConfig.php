<?php

use App\Loguzz\Formatter\RequestJsonFormatter;

return [
    "serviceProvider" => "",
    "serviceVersion" => "",
    "serviceUrl" => "",
    "serviceConntectTimout" => "",
    "serviceReadTimout" => "",
    "serviceUserName" => "",
    "servicePassword" => "",
    "loggerOption" => [
        'length' => 200,
        'log_request' => true,
        'log_response' => true,
        'log_level' => 'info',
        'force_json' => false,
        'request_formatter' => new RequestJsonFormatter,
        'separate' => true,
    ]

];
